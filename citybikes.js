// http://realnorth.net/blog/json-qml/

function getNetworks(onReady, onFail){
    var xhr = new XMLHttpRequest;
    xhr.open("GET", "http://api.citybik.es/networks.json");
    xhr.onreadystatechange = function (){
        switch (xhr.readyState){
            case XMLHttpRequest.DONE:
                onReady(xhr.responseText);
                break;
            default:
                onFail(xhr.readyState);
                break;
        }
    }
    xhr.send();
}

function getStation(system, onReady, onFail){
    var xhr = new XMLHttpRequest;
    xhr.open("GET", "http://api.citybik.es/"+system+".json");
    xhr.onreadystatechange = function (){
        switch (xhr.readyState){
            case XMLHttpRequest.DONE:
                onReady(xhr.responseText);
                break;
            default:
                onFail(xhr.readyState);
                break;
        }
    }
    xhr.send();
}
