import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtPositioning 5.2
import QtLocation 5.3
import QtQml 2.2
import "citybikes.js" as CityBikes

ApplicationWindow
{
    id: root
    width: 360
    height: 360
    readonly property real magnitude: 1000000

    function networksReceived(data)
    {
        console.log('received systems!');

        var obj = JSON.parse(data);

        for(var o in obj) {
            var current = obj[o];

            systemsModel.append(current)
        }
    }

    function networksReceptionStateChanged(data)
    {
        switch(data) {
            case XMLHttpRequest.LOADING:
                console.log('loading');
                break;
            case XMLHttpRequest.HEADERS_RECEIVED:
                console.log('receiving data...');
                break;
            default:
                console.log("status networks! "+data);
                break;
        }
    }

    function systemReceptionStateChanged(data)
    {
        switch(data) {
            case XMLHttpRequest.LOADING:
                console.log('loading system');
                break;
            case XMLHttpRequest.HEADERS_RECEIVED:
                console.log('receiving system data...');
                break;
            default:
                console.log("status system! "+data);
                break;
        }
    }

    function systemReceived(data)
    {
        console.log(systemsModel.get(systemsView.currentIndex).name+' ready')
        stationsModel.clear();

        var obj = JSON.parse(data);
        theMap.clearMapItems();

        for(var o in obj) {
            var current = obj[o];
            current.display = current.name + ": " + current.bikes + " / " + current.free;
            stationsModel.append(current)
        }
    }

    ListModel {
        id: systemsModel
    }

    ListModel {
        id: stationsModel
    }

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            Button {
                id:button
                text: "Start!"
                onClicked: {
                    CityBikes.getNetworks(networksReceived, networksReceptionStateChanged);
                    console.log("fetching...")
                }
            }

            ComboBox  {
                id: systemsView
                model: systemsModel
                Layout.fillWidth: true
                textRole: "name"

                onCurrentIndexChanged: {
                    var data = systemsModel.get(currentIndex);
                    if (!data)
                        return;

                    theMap.center = QtPositioning.coordinate(data.lat/magnitude, data.lng/magnitude);

                    var systemName = data.name;
                    CityBikes.getStation(systemName, systemReceived, systemReceptionStateChanged);
                    console.log("fetching ", systemName);
                }
            }
            CheckBox {
                id: grabbing
            }
        }

        Map {
            id: theMap
            Layout.fillHeight: true
            Layout.fillWidth: true

            plugin: Plugin { name: "osm" }

            zoomLevel: 13

            gesture.enabled: true
        }

        Instantiator {
            model: stationsModel
            delegate: MapCircle {
                center: QtPositioning.coordinate(lat/magnitude, lng/magnitude)
                radius: 100
                color: grabbing.checked == (free>bikes) ? "green" : "red"
                border.width: 1
            }
            onObjectAdded: theMap.addMapItem(object)
            onObjectRemoved: theMap.removeMapItem(object)
        }
    }
}
